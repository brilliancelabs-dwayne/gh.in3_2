<?php

class AppModel extends Model {

	public $recursive = -1; //default tweak to improve speed

	//Add to blAC distibution
	function beforeFind($d){

		switch($this->name):

			case "Person":				
				//make the new field available for use in the list
				if(is_array($d['fields'])): //Fix that ensures only two value lists are changed
					//build the concatenated value
					$this->virtualFields = array(
						"displayName" => "CONCAT(Person.FirstName,' ',
							Person.LastName,' [',Person.id,']')"		
					);

					//set the cake displayfield to the new value
					$this->displayField = "displayName";

					$d['fields'][1] = "Person.displayName";
				
					//change the lists value field to show the new value			
					$d['list']['valuePath'] = "{n}.Person.displayName";

				endif;

			break;
		
			case "Employee":

				if(is_array($d['fields'])):
					$this->recursive = 2;
					$this->Person->recursive = 2;
					$d['recursive'] = 2;

					$this->virtualFields = array(
						"displayName" => 'CONCAT(Person.FirstName," ",Person.LastName," - Employee ID: ",Employee.id)'		
					);

					//set the cake displayfield to the new value
					$this->displayField = "displayName";

					$d['fields'][1] = "Employee.displayName";
				
					//change the lists value field to show the new value			
					$d['list']['valuePath'] = "{n}.Employee.displayName";

				endif;

			break;
	
			default:


		endswitch;

		//Return the value back to the find method whether altered or not
		return $d;
	}

	private function get_list($model = null, $new_id = null){
		if($model):
			$m_obj = ClassRegistry::init($model);
			return $m_obj->find('list');
		endif;
	}
}
