<?php
/**CakePHP 1.3 > Altered by blAC**/

class AppController extends Controller {
	var $helpers = array(
		'Html', 
		'Form', 
		'Session'
	);

	var $components = array(
		'Session',
		'RequestHandler',
		//Add a comment to the next line to de-activate the Auth component
		'Auth'
	);


	function beforeFilter(){

		if(!$this->RequestHandler->isAjax()):
			//Generic Naming patterns base on containing folder name
			//Change here manually if you please
			$appName = @$_SESSION['appName'];
			$tagLine = @$_SESSION['tagLine'];
			$poster  = @$_SESSION['poster'];

			//Set authenticated user data if it is available
			$user = $this->_setUser();
		
			//Send the vars to the view
			$this->set(compact('appName', 'tagLine', 'poster', 'user'));
		endif;

		/**START CUSTOM CODE HERE**/


	}


/******===============
*******===============
***blAC METHODS=======
======================
======================
====================*/


	//If the Auth component is used find the authenticated user data and return it
	private function _setUser(){
		if(isset($this->Auth)):

			//tell the view that the AuthStatus bar will be used
			$this->set('AuthStatus', 1);

			//get the auth user data
			$u = @$this->Auth->User();
			if(is_array($u)):
				return @$u['User'];
			
			endif;


		else:
			//tell the view that the AuthStatus bar will not be used
			$this->set('AuthStatus', 0);
			if($this->params['action']=='login'):
				$this->Session->setFlash('This application does not require authentication');
				$this->redirect(array('controller' => 'pages', 'action' => 'display', 'home'));
			endif;
		endif;
	}

	public function deactivate($model = null, $record = null, $status = 0){
		//simple polymorphic f(x) to deactivate or activate a field
		$this->loadModel($model);
		$this->$model->read(null, $record);
		$this->$model->set("Active",$status);
		if($this->$model->save()){
			return true;	
		}else{
			return false;
		}

	}
	/**Trial inclusion into blAC though it might be too specific**/
	public function get_user_for_list($model = null, $model_id = null){
		$this->autoRender = false;
		$this->layout = 'ajax';
		if($this->RequestHandler->isAjax()){
			$this->loadModel($model);
			$find_person = $this->$model->find('list', array(
				'fields' => array($model.'.id', $model.'.person_id'),
				'conditions' => array($model.'.id' => $model_id),
				'recursive' => 0
			));
			$person = $find_person[$model_id];
			$this->loadModel('Person');
			$find_data = $this->Person->find('list', array(
				'fields' => array('id', 'FullName'),
				'conditions' => array('id' => $person),
				'recursive' => 0
			));
			$pdata = $find_data[$person];
			echo $pdata;
			
		}else{
			echo "Person not found";
		}		
	}

}
