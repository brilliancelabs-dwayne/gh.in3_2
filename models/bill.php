<?php
class Bill extends AppModel {
	var $name = 'Bill';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'Balance' => array(
			'className' => 'Balance',
			'foreignKey' => 'bill_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'BillPayment' => array(
			'className' => 'BillPayment',
			'foreignKey' => 'bill_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
