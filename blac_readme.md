BLAC Styles uses all the typical CakePHP HTML elements and provides an alternative styling for the protyping system

CSS Features:

1) Colors:
	- Basic Gray, White, Dark Gray, and Royalblue colors are used for fonts and backgrounds

2) Basic Elements
	- Header elements create inline elements that are easy to use
	- #header h2#logo tag will put your logo or logo text into the top-left corner
	- #header h3#tagline tag will place tagline in a rounded royalblue box next to the logo 

3) Major Style Updates
	- View (or READ) pages have a new cool looking layout that includes tag clouds below related tables

JS Features:

1) jQuery is used to auto-hide or remove pesky fields and to re-style others
	- Actions side bar is transformed into an actions tag cloud
	- Submit button text renamed to "Save"
	- removeCol function removes ugly db columns like "Created", "Updated", and "Active"
	- Ajax get_person_list will automatically list the name of the selected person from a drop-down in a div next to the drop-down
