<div class="progManagers view">
<h2><?php  __('Prog Manager');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progManager['ProgManager']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Prog'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($progManager['Prog']['id'], array('controller' => 'progs', 'action' => 'view', $progManager['Prog']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Manager'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($progManager['Manager']['id'], array('controller' => 'managers', 'action' => 'view', $progManager['Manager']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Note'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progManager['ProgManager']['Note']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Prog Manager', true), array('action' => 'edit', $progManager['ProgManager']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Prog Manager', true), array('action' => 'delete', $progManager['ProgManager']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progManager['ProgManager']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Managers', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Manager', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Managers', true), array('controller' => 'managers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Manager', true), array('controller' => 'managers', 'action' => 'add')); ?> </li>
	</ul>
</div>
