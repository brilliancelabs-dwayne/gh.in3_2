<div class="progManagers form">
<?php echo $this->Form->create('ProgManager');?>
	<fieldset>
		<legend><?php __('Edit Prog Manager'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('prog_id');
		echo $this->Form->input('manager_id');
		echo $this->Form->input('Note');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ProgManager.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ProgManager.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Prog Managers', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Managers', true), array('controller' => 'managers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Manager', true), array('controller' => 'managers', 'action' => 'add')); ?> </li>
	</ul>
</div>