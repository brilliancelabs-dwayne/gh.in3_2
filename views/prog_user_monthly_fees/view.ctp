<div class="progUserMonthlyFees view">
<h2><?php  __('Prog User Monthly Fee');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Prog'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($progUserMonthlyFee['Prog']['id'], array('controller' => 'progs', 'action' => 'view', $progUserMonthlyFee['Prog']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($progUserMonthlyFee['User']['id'], array('controller' => 'users', 'action' => 'view', $progUserMonthlyFee['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Prog User Monthly Fee', true), array('action' => 'edit', $progUserMonthlyFee['ProgUserMonthlyFee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Prog User Monthly Fee', true), array('action' => 'delete', $progUserMonthlyFee['ProgUserMonthlyFee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progUserMonthlyFee['ProgUserMonthlyFee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog User Monthly Fees', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog User Monthly Fee', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
