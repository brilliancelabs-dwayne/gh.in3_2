<div class="progUserMonthlyFees index">
	<h2><?php __('Prog User Monthly Fees');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('prog_id');?></th>
			<th><?php echo $this->Paginator->sort('user_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Amount');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($progUserMonthlyFees as $progUserMonthlyFee):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($progUserMonthlyFee['Prog']['id'], array('controller' => 'progs', 'action' => 'view', $progUserMonthlyFee['Prog']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($progUserMonthlyFee['User']['id'], array('controller' => 'users', 'action' => 'view', $progUserMonthlyFee['User']['id'])); ?>
		</td>
		<td><?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Name']; ?>&nbsp;</td>
		<td><?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Amount']; ?>&nbsp;</td>
		<td><?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Active']; ?>&nbsp;</td>
		<td><?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Created']; ?>&nbsp;</td>
		<td><?php echo $progUserMonthlyFee['ProgUserMonthlyFee']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $progUserMonthlyFee['ProgUserMonthlyFee']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $progUserMonthlyFee['ProgUserMonthlyFee']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $progUserMonthlyFee['ProgUserMonthlyFee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progUserMonthlyFee['ProgUserMonthlyFee']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Prog User Monthly Fee', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>