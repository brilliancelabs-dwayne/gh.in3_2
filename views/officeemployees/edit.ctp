<div class="officeemployees form">
<?php echo $this->Form->create('Officeemployee');?>
	<fieldset>
		<legend><?php __('Edit Officeemployee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('employee_id');
		echo $this->Form->input('Note');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Officeemployee.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Officeemployee.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Officeemployees', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Employees', true), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee', true), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>