<div class="officeemployees view">
<h2><?php  __('Officeemployee');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $officeemployee['Officeemployee']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Employee'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($officeemployee['Employee']['id'], array('controller' => 'employees', 'action' => 'view', $officeemployee['Employee']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Note'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $officeemployee['Officeemployee']['Note']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Officeemployee', true), array('action' => 'edit', $officeemployee['Officeemployee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Officeemployee', true), array('action' => 'delete', $officeemployee['Officeemployee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $officeemployee['Officeemployee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Officeemployees', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Officeemployee', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees', true), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee', true), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>
