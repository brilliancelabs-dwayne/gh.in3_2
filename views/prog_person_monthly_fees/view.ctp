<div class="progPersonMonthlyFees view">
<h2><?php  __('Prog Person Monthly Fee');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progPersonMonthlyFee['ProgPersonMonthlyFee']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Prog'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($progPersonMonthlyFee['Prog']['id'], array('controller' => 'progs', 'action' => 'view', $progPersonMonthlyFee['Prog']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Person'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($progPersonMonthlyFee['Person']['id'], array('controller' => 'people', 'action' => 'view', $progPersonMonthlyFee['Person']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progPersonMonthlyFee['ProgPersonMonthlyFee']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progPersonMonthlyFee['ProgPersonMonthlyFee']['Amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progPersonMonthlyFee['ProgPersonMonthlyFee']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progPersonMonthlyFee['ProgPersonMonthlyFee']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progPersonMonthlyFee['ProgPersonMonthlyFee']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Prog Person Monthly Fee', true), array('action' => 'edit', $progPersonMonthlyFee['ProgPersonMonthlyFee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Prog Person Monthly Fee', true), array('action' => 'delete', $progPersonMonthlyFee['ProgPersonMonthlyFee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progPersonMonthlyFee['ProgPersonMonthlyFee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Person Monthly Fees', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Person Monthly Fee', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List People', true), array('controller' => 'people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add')); ?> </li>
	</ul>
</div>
