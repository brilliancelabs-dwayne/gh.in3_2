<div class="progPersonMonthlyFees form">
<?php echo $this->Form->create('ProgPersonMonthlyFee');?>
	<fieldset>
		<legend><?php __('Add Prog Person Monthly Fee'); ?></legend>
	<?php
		echo $this->Form->input('prog_id');
		echo $this->Form->input('person_id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Amount');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Prog Person Monthly Fees', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List People', true), array('controller' => 'people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add')); ?> </li>
	</ul>
</div>