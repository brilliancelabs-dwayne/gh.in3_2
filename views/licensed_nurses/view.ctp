<div class="licensedNurses view">
<h2><?php  __('Licensed Nurse');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $licensedNurse['LicensedNurse']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Employee'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($licensedNurse['Employee']['id'], array('controller' => 'employees', 'action' => 'view', $licensedNurse['Employee']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Note'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $licensedNurse['LicensedNurse']['Note']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Licensed Nurse', true), array('action' => 'edit', $licensedNurse['LicensedNurse']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Licensed Nurse', true), array('action' => 'delete', $licensedNurse['LicensedNurse']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $licensedNurse['LicensedNurse']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Licensed Nurses', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Licensed Nurse', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees', true), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee', true), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>
