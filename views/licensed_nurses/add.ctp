<div class="licensedNurses form">
<?php echo $this->Form->create('LicensedNurse');?>
	<fieldset>
		<legend><?php __('Add Licensed Nurse'); ?></legend>
	<?php
		echo $this->Form->input('employee_id');
		echo $this->Form->input('Note');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Licensed Nurses', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Employees', true), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee', true), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>