<div class="balances view">
<h2><?php  __('Balance');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $balance['Balance']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Bill'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($balance['Bill']['id'], array('controller' => 'bills', 'action' => 'view', $balance['Bill']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Bill Payment'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($balance['BillPayment']['id'], array('controller' => 'bill_payments', 'action' => 'view', $balance['BillPayment']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $balance['Balance']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $balance['Balance']['Amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $balance['Balance']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $balance['Balance']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $balance['Balance']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Balance', true), array('action' => 'edit', $balance['Balance']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Balance', true), array('action' => 'delete', $balance['Balance']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $balance['Balance']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Balances', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Balance', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bills', true), array('controller' => 'bills', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill', true), array('controller' => 'bills', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bill Payments', true), array('controller' => 'bill_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill Payment', true), array('controller' => 'bill_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>
