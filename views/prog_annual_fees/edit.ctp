<div class="progAnnualFees form">
<?php echo $this->Form->create('ProgAnnualFee');?>
	<fieldset>
		<legend><?php __('Edit Prog Annual Fee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('prog_id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Amount');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ProgAnnualFee.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ProgAnnualFee.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Prog Annual Fees', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
	</ul>
</div>