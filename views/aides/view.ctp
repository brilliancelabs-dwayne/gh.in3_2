<div class="aides view">
<h2><?php  __('Aide');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $aide['Aide']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Employee'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($aide['Employee']['id'], array('controller' => 'employees', 'action' => 'view', $aide['Employee']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Note'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $aide['Aide']['Note']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Aide', true), array('action' => 'edit', $aide['Aide']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Aide', true), array('action' => 'delete', $aide['Aide']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $aide['Aide']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Aides', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aide', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees', true), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee', true), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>
