<div class="employees view">
<h2><?php  __('Employee');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $employee['Employee']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Person'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($employee['Person']['id'], array('controller' => 'people', 'action' => 'view', $employee['Person']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Note'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $employee['Employee']['Note']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Employee', true), array('action' => 'edit', $employee['Employee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Employee', true), array('action' => 'delete', $employee['Employee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $employee['Employee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List People', true), array('controller' => 'people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aides', true), array('controller' => 'aides', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aide', true), array('controller' => 'aides', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Licensed Nurses', true), array('controller' => 'licensed_nurses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Licensed Nurse', true), array('controller' => 'licensed_nurses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Officeemployees', true), array('controller' => 'officeemployees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Officeemployee', true), array('controller' => 'officeemployees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Registered Nurses', true), array('controller' => 'registered_nurses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Registered Nurse', true), array('controller' => 'registered_nurses', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Aides');?></h3>
	<?php if (!empty($employee['Aide'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Employee Id'); ?></th>
		<th><?php __('Note'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($employee['Aide'] as $aide):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $aide['id'];?></td>
			<td><?php echo $aide['employee_id'];?></td>
			<td><?php echo $aide['Note'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'aides', 'action' => 'view', $aide['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'aides', 'action' => 'edit', $aide['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'aides', 'action' => 'delete', $aide['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $aide['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Aide', true), array('controller' => 'aides', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Licensed Nurses');?></h3>
	<?php if (!empty($employee['LicensedNurse'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Employee Id'); ?></th>
		<th><?php __('Note'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($employee['LicensedNurse'] as $licensedNurse):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $licensedNurse['id'];?></td>
			<td><?php echo $licensedNurse['employee_id'];?></td>
			<td><?php echo $licensedNurse['Note'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'licensed_nurses', 'action' => 'view', $licensedNurse['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'licensed_nurses', 'action' => 'edit', $licensedNurse['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'licensed_nurses', 'action' => 'delete', $licensedNurse['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $licensedNurse['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Licensed Nurse', true), array('controller' => 'licensed_nurses', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Officeemployees');?></h3>
	<?php if (!empty($employee['Officeemployee'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Employee Id'); ?></th>
		<th><?php __('Note'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($employee['Officeemployee'] as $officeemployee):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $officeemployee['id'];?></td>
			<td><?php echo $officeemployee['employee_id'];?></td>
			<td><?php echo $officeemployee['Note'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'officeemployees', 'action' => 'view', $officeemployee['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'officeemployees', 'action' => 'edit', $officeemployee['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'officeemployees', 'action' => 'delete', $officeemployee['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $officeemployee['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Officeemployee', true), array('controller' => 'officeemployees', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Registered Nurses');?></h3>
	<?php if (!empty($employee['RegisteredNurse'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Employee Id'); ?></th>
		<th><?php __('Note'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($employee['RegisteredNurse'] as $registeredNurse):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $registeredNurse['id'];?></td>
			<td><?php echo $registeredNurse['employee_id'];?></td>
			<td><?php echo $registeredNurse['Note'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'registered_nurses', 'action' => 'view', $registeredNurse['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'registered_nurses', 'action' => 'edit', $registeredNurse['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'registered_nurses', 'action' => 'delete', $registeredNurse['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $registeredNurse['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Registered Nurse', true), array('controller' => 'registered_nurses', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
