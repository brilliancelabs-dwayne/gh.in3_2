<div class="employees form">
<?php echo $this->Form->create('Employee');?>
	<fieldset>
		<legend><?php __('Edit Employee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('person_id');
		echo $this->Form->input('Note');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Employee.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Employee.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Employees', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List People', true), array('controller' => 'people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aides', true), array('controller' => 'aides', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aide', true), array('controller' => 'aides', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Licensed Nurses', true), array('controller' => 'licensed_nurses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Licensed Nurse', true), array('controller' => 'licensed_nurses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Officeemployees', true), array('controller' => 'officeemployees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Officeemployee', true), array('controller' => 'officeemployees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Registered Nurses', true), array('controller' => 'registered_nurses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Registered Nurse', true), array('controller' => 'registered_nurses', 'action' => 'add')); ?> </li>
	</ul>
</div>