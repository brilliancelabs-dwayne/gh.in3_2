<div class="employees index">
	<h2><?php __('Employees');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('person_id');?></th>
			<th><?php echo $this->Paginator->sort('Note');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($employees as $employee):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $employee['Employee']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($employee['Person']['id'], array('controller' => 'people', 'action' => 'view', $employee['Person']['id'])); ?>
		</td>
		<td><?php echo $employee['Employee']['Note']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $employee['Employee']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $employee['Employee']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $employee['Employee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $employee['Employee']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Employee', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List People', true), array('controller' => 'people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aides', true), array('controller' => 'aides', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aide', true), array('controller' => 'aides', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Licensed Nurses', true), array('controller' => 'licensed_nurses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Licensed Nurse', true), array('controller' => 'licensed_nurses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Officeemployees', true), array('controller' => 'officeemployees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Officeemployee', true), array('controller' => 'officeemployees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Registered Nurses', true), array('controller' => 'registered_nurses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Registered Nurse', true), array('controller' => 'registered_nurses', 'action' => 'add')); ?> </li>
	</ul>
</div>
