<div class="progs view">
<h2><?php  __('Prog');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $prog['Prog']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $prog['Prog']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $prog['Prog']['Description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $prog['Prog']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $prog['Prog']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $prog['Prog']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Prog', true), array('action' => 'edit', $prog['Prog']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Prog', true), array('action' => 'delete', $prog['Prog']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $prog['Prog']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Annual Fees', true), array('controller' => 'prog_annual_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Annual Fee', true), array('controller' => 'prog_annual_fees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Managers', true), array('controller' => 'prog_managers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Manager', true), array('controller' => 'prog_managers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Monthly Fees', true), array('controller' => 'prog_monthly_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Monthly Fee', true), array('controller' => 'prog_monthly_fees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog People', true), array('controller' => 'prog_people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Person', true), array('controller' => 'prog_people', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Person Monthly Fees', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Person Monthly Fee', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog User Monthly Fees', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog User Monthly Fee', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Prog Annual Fees');?></h3>
	<?php if (!empty($prog['ProgAnnualFee'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Prog Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($prog['ProgAnnualFee'] as $progAnnualFee):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $progAnnualFee['id'];?></td>
			<td><?php echo $progAnnualFee['prog_id'];?></td>
			<td><?php echo $progAnnualFee['Name'];?></td>
			<td><?php echo $progAnnualFee['Amount'];?></td>
			<td><?php echo $progAnnualFee['Active'];?></td>
			<td><?php echo $progAnnualFee['Created'];?></td>
			<td><?php echo $progAnnualFee['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'prog_annual_fees', 'action' => 'view', $progAnnualFee['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'prog_annual_fees', 'action' => 'edit', $progAnnualFee['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'prog_annual_fees', 'action' => 'delete', $progAnnualFee['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progAnnualFee['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Prog Annual Fee', true), array('controller' => 'prog_annual_fees', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Prog Managers');?></h3>
	<?php if (!empty($prog['ProgManager'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Prog Id'); ?></th>
		<th><?php __('Manager Id'); ?></th>
		<th><?php __('Note'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($prog['ProgManager'] as $progManager):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $progManager['id'];?></td>
			<td><?php echo $progManager['prog_id'];?></td>
			<td><?php echo $progManager['manager_id'];?></td>
			<td><?php echo $progManager['Note'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'prog_managers', 'action' => 'view', $progManager['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'prog_managers', 'action' => 'edit', $progManager['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'prog_managers', 'action' => 'delete', $progManager['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progManager['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Prog Manager', true), array('controller' => 'prog_managers', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Prog Monthly Fees');?></h3>
	<?php if (!empty($prog['ProgMonthlyFee'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Prog Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($prog['ProgMonthlyFee'] as $progMonthlyFee):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $progMonthlyFee['id'];?></td>
			<td><?php echo $progMonthlyFee['prog_id'];?></td>
			<td><?php echo $progMonthlyFee['Name'];?></td>
			<td><?php echo $progMonthlyFee['Amount'];?></td>
			<td><?php echo $progMonthlyFee['Active'];?></td>
			<td><?php echo $progMonthlyFee['Created'];?></td>
			<td><?php echo $progMonthlyFee['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'prog_monthly_fees', 'action' => 'view', $progMonthlyFee['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'prog_monthly_fees', 'action' => 'edit', $progMonthlyFee['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'prog_monthly_fees', 'action' => 'delete', $progMonthlyFee['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progMonthlyFee['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Prog Monthly Fee', true), array('controller' => 'prog_monthly_fees', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Prog People');?></h3>
	<?php if (!empty($prog['ProgPerson'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Prog Id'); ?></th>
		<th><?php __('Person Id'); ?></th>
		<th><?php __('Note'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($prog['ProgPerson'] as $progPerson):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $progPerson['id'];?></td>
			<td><?php echo $progPerson['prog_id'];?></td>
			<td><?php echo $progPerson['person_id'];?></td>
			<td><?php echo $progPerson['Note'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'prog_people', 'action' => 'view', $progPerson['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'prog_people', 'action' => 'edit', $progPerson['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'prog_people', 'action' => 'delete', $progPerson['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progPerson['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Prog Person', true), array('controller' => 'prog_people', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Prog Person Monthly Fees');?></h3>
	<?php if (!empty($prog['ProgPersonMonthlyFee'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Prog Id'); ?></th>
		<th><?php __('Person Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($prog['ProgPersonMonthlyFee'] as $progPersonMonthlyFee):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $progPersonMonthlyFee['id'];?></td>
			<td><?php echo $progPersonMonthlyFee['prog_id'];?></td>
			<td><?php echo $progPersonMonthlyFee['person_id'];?></td>
			<td><?php echo $progPersonMonthlyFee['Name'];?></td>
			<td><?php echo $progPersonMonthlyFee['Amount'];?></td>
			<td><?php echo $progPersonMonthlyFee['Active'];?></td>
			<td><?php echo $progPersonMonthlyFee['Created'];?></td>
			<td><?php echo $progPersonMonthlyFee['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'view', $progPersonMonthlyFee['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'edit', $progPersonMonthlyFee['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'delete', $progPersonMonthlyFee['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progPersonMonthlyFee['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Prog Person Monthly Fee', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Prog User Monthly Fees');?></h3>
	<?php if (!empty($prog['ProgUserMonthlyFee'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Prog Id'); ?></th>
		<th><?php __('User Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($prog['ProgUserMonthlyFee'] as $progUserMonthlyFee):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $progUserMonthlyFee['id'];?></td>
			<td><?php echo $progUserMonthlyFee['prog_id'];?></td>
			<td><?php echo $progUserMonthlyFee['user_id'];?></td>
			<td><?php echo $progUserMonthlyFee['Name'];?></td>
			<td><?php echo $progUserMonthlyFee['Amount'];?></td>
			<td><?php echo $progUserMonthlyFee['Active'];?></td>
			<td><?php echo $progUserMonthlyFee['Created'];?></td>
			<td><?php echo $progUserMonthlyFee['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'view', $progUserMonthlyFee['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'edit', $progUserMonthlyFee['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'delete', $progUserMonthlyFee['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progUserMonthlyFee['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Prog User Monthly Fee', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
