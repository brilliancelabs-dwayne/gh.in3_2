<div class="progs form">
<?php echo $this->Form->create('Prog');?>
	<fieldset>
		<legend><?php __('Edit Prog'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Description');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Prog.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Prog.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Prog Annual Fees', true), array('controller' => 'prog_annual_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Annual Fee', true), array('controller' => 'prog_annual_fees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Managers', true), array('controller' => 'prog_managers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Manager', true), array('controller' => 'prog_managers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Monthly Fees', true), array('controller' => 'prog_monthly_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Monthly Fee', true), array('controller' => 'prog_monthly_fees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog People', true), array('controller' => 'prog_people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Person', true), array('controller' => 'prog_people', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Person Monthly Fees', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Person Monthly Fee', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog User Monthly Fees', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog User Monthly Fee', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'add')); ?> </li>
	</ul>
</div>