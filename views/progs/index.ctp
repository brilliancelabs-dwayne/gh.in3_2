<div class="progs index">
	<h2><?php __('Progs');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Description');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($progs as $prog):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $prog['Prog']['id']; ?>&nbsp;</td>
		<td><?php echo $prog['Prog']['Name']; ?>&nbsp;</td>
		<td><?php echo $prog['Prog']['Description']; ?>&nbsp;</td>
		<td><?php echo $prog['Prog']['Active']; ?>&nbsp;</td>
		<td><?php echo $prog['Prog']['Created']; ?>&nbsp;</td>
		<td><?php echo $prog['Prog']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $prog['Prog']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $prog['Prog']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $prog['Prog']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $prog['Prog']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Prog', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Prog Annual Fees', true), array('controller' => 'prog_annual_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Annual Fee', true), array('controller' => 'prog_annual_fees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Managers', true), array('controller' => 'prog_managers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Manager', true), array('controller' => 'prog_managers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Monthly Fees', true), array('controller' => 'prog_monthly_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Monthly Fee', true), array('controller' => 'prog_monthly_fees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog People', true), array('controller' => 'prog_people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Person', true), array('controller' => 'prog_people', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Person Monthly Fees', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Person Monthly Fee', true), array('controller' => 'prog_person_monthly_fees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog User Monthly Fees', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog User Monthly Fee', true), array('controller' => 'prog_user_monthly_fees', 'action' => 'add')); ?> </li>
	</ul>
</div>