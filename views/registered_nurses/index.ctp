<div class="registeredNurses index">
	<h2><?php __('Registered Nurses');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('employee_id');?></th>
			<th><?php echo $this->Paginator->sort('Note');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($registeredNurses as $registeredNurse):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $registeredNurse['RegisteredNurse']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($registeredNurse['Employee']['id'], array('controller' => 'employees', 'action' => 'view', $registeredNurse['Employee']['id'])); ?>
		</td>
		<td><?php echo $registeredNurse['RegisteredNurse']['Note']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $registeredNurse['RegisteredNurse']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $registeredNurse['RegisteredNurse']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $registeredNurse['RegisteredNurse']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $registeredNurse['RegisteredNurse']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Registered Nurse', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Employees', true), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee', true), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>