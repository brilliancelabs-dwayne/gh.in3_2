<div class="registeredNurses view">
<h2><?php  __('Registered Nurse');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $registeredNurse['RegisteredNurse']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Employee'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($registeredNurse['Employee']['id'], array('controller' => 'employees', 'action' => 'view', $registeredNurse['Employee']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Note'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $registeredNurse['RegisteredNurse']['Note']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Registered Nurse', true), array('action' => 'edit', $registeredNurse['RegisteredNurse']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Registered Nurse', true), array('action' => 'delete', $registeredNurse['RegisteredNurse']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $registeredNurse['RegisteredNurse']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Registered Nurses', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Registered Nurse', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employees', true), array('controller' => 'employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee', true), array('controller' => 'employees', 'action' => 'add')); ?> </li>
	</ul>
</div>
