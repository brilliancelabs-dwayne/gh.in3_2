<div class="users form">
<?php echo $this->Form->create('User');?>
	<fieldset>
		<legend><?php __('Register'); ?></legend>
	<?php
		echo $this->Form->input('Email');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('FirstName');
		echo $this->Form->input('MiddleInitial');
		echo $this->Form->input('LastName');
		echo $this->Form->input('Suffix');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Login', true), array('action' => 'login'));?></li>
	</ul>
</div>
