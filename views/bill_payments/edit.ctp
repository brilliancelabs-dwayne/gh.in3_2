<div class="billPayments form">
<?php echo $this->Form->create('BillPayment');?>
	<fieldset>
		<legend><?php __('Edit Bill Payment'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('bill_id');
		echo $this->Form->input('merchant_id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Amount');
		echo $this->Form->input('Active');
		echo $this->Form->input('TransactionId');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('BillPayment.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('BillPayment.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Bill Payments', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Bills', true), array('controller' => 'bills', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill', true), array('controller' => 'bills', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Merchants', true), array('controller' => 'merchants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Merchant', true), array('controller' => 'merchants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Balances', true), array('controller' => 'balances', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Balance', true), array('controller' => 'balances', 'action' => 'add')); ?> </li>
	</ul>
</div>