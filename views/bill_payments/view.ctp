<div class="billPayments view">
<h2><?php  __('Bill Payment');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $billPayment['BillPayment']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Bill'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($billPayment['Bill']['id'], array('controller' => 'bills', 'action' => 'view', $billPayment['Bill']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Merchant'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($billPayment['Merchant']['id'], array('controller' => 'merchants', 'action' => 'view', $billPayment['Merchant']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $billPayment['BillPayment']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $billPayment['BillPayment']['Amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $billPayment['BillPayment']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('TransactionId'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $billPayment['BillPayment']['TransactionId']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $billPayment['BillPayment']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $billPayment['BillPayment']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bill Payment', true), array('action' => 'edit', $billPayment['BillPayment']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Bill Payment', true), array('action' => 'delete', $billPayment['BillPayment']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $billPayment['BillPayment']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bill Payments', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill Payment', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bills', true), array('controller' => 'bills', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill', true), array('controller' => 'bills', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Merchants', true), array('controller' => 'merchants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Merchant', true), array('controller' => 'merchants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Balances', true), array('controller' => 'balances', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Balance', true), array('controller' => 'balances', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Balances');?></h3>
	<?php if (!empty($billPayment['Balance'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Bill Id'); ?></th>
		<th><?php __('Bill Payment Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($billPayment['Balance'] as $balance):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $balance['id'];?></td>
			<td><?php echo $balance['bill_id'];?></td>
			<td><?php echo $balance['bill_payment_id'];?></td>
			<td><?php echo $balance['Name'];?></td>
			<td><?php echo $balance['Amount'];?></td>
			<td><?php echo $balance['Active'];?></td>
			<td><?php echo $balance['Created'];?></td>
			<td><?php echo $balance['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'balances', 'action' => 'view', $balance['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'balances', 'action' => 'edit', $balance['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'balances', 'action' => 'delete', $balance['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $balance['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Balance', true), array('controller' => 'balances', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
