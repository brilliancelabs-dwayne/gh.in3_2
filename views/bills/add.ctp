<div class="bills form">
<?php echo $this->Form->create('Bill');?>
	<fieldset>
		<legend><?php __('Add Bill'); ?></legend>
	<?php
		echo $this->Form->input('Name');
		echo $this->Form->input('EventDate');
		echo $this->Form->input('Description');
		echo $this->Form->input('Active');
		echo $this->Form->input('Amount');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Bills', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Balances', true), array('controller' => 'balances', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Balance', true), array('controller' => 'balances', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bill Payments', true), array('controller' => 'bill_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill Payment', true), array('controller' => 'bill_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>