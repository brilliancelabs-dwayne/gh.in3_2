<div class="bills index">
	<h2><?php __('Bills');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('EventDate');?></th>
			<th><?php echo $this->Paginator->sort('Description');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Amount');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($bills as $bill):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $bill['Bill']['id']; ?>&nbsp;</td>
		<td><?php echo $bill['Bill']['Name']; ?>&nbsp;</td>
		<td><?php echo $bill['Bill']['EventDate']; ?>&nbsp;</td>
		<td><?php echo $bill['Bill']['Description']; ?>&nbsp;</td>
		<td><?php echo $bill['Bill']['Active']; ?>&nbsp;</td>
		<td><?php echo $bill['Bill']['Amount']; ?>&nbsp;</td>
		<td><?php echo $bill['Bill']['Created']; ?>&nbsp;</td>
		<td><?php echo $bill['Bill']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $bill['Bill']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $bill['Bill']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $bill['Bill']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bill['Bill']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bill', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Balances', true), array('controller' => 'balances', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Balance', true), array('controller' => 'balances', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bill Payments', true), array('controller' => 'bill_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill Payment', true), array('controller' => 'bill_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>