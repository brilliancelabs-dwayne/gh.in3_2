<div class="bills view">
<h2><?php  __('Bill');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bill['Bill']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bill['Bill']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('EventDate'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bill['Bill']['EventDate']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bill['Bill']['Description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bill['Bill']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bill['Bill']['Amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bill['Bill']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bill['Bill']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bill', true), array('action' => 'edit', $bill['Bill']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Bill', true), array('action' => 'delete', $bill['Bill']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bill['Bill']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bills', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Balances', true), array('controller' => 'balances', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Balance', true), array('controller' => 'balances', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bill Payments', true), array('controller' => 'bill_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill Payment', true), array('controller' => 'bill_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Balances');?></h3>
	<?php if (!empty($bill['Balance'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Bill Id'); ?></th>
		<th><?php __('Bill Payment Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($bill['Balance'] as $balance):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $balance['id'];?></td>
			<td><?php echo $balance['bill_id'];?></td>
			<td><?php echo $balance['bill_payment_id'];?></td>
			<td><?php echo $balance['Name'];?></td>
			<td><?php echo $balance['Amount'];?></td>
			<td><?php echo $balance['Active'];?></td>
			<td><?php echo $balance['Created'];?></td>
			<td><?php echo $balance['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'balances', 'action' => 'view', $balance['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'balances', 'action' => 'edit', $balance['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'balances', 'action' => 'delete', $balance['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $balance['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Balance', true), array('controller' => 'balances', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Bill Payments');?></h3>
	<?php if (!empty($bill['BillPayment'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Bill Id'); ?></th>
		<th><?php __('Merchant Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('TransactionId'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($bill['BillPayment'] as $billPayment):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $billPayment['id'];?></td>
			<td><?php echo $billPayment['bill_id'];?></td>
			<td><?php echo $billPayment['merchant_id'];?></td>
			<td><?php echo $billPayment['Name'];?></td>
			<td><?php echo $billPayment['Amount'];?></td>
			<td><?php echo $billPayment['Active'];?></td>
			<td><?php echo $billPayment['TransactionId'];?></td>
			<td><?php echo $billPayment['Created'];?></td>
			<td><?php echo $billPayment['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bill_payments', 'action' => 'view', $billPayment['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bill_payments', 'action' => 'edit', $billPayment['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bill_payments', 'action' => 'delete', $billPayment['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $billPayment['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bill Payment', true), array('controller' => 'bill_payments', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
