<div class="progMonthlyFees index">
	<h2><?php __('Prog Monthly Fees');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('prog_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Amount');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($progMonthlyFees as $progMonthlyFee):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $progMonthlyFee['ProgMonthlyFee']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($progMonthlyFee['Prog']['id'], array('controller' => 'progs', 'action' => 'view', $progMonthlyFee['Prog']['id'])); ?>
		</td>
		<td><?php echo $progMonthlyFee['ProgMonthlyFee']['Name']; ?>&nbsp;</td>
		<td><?php echo $progMonthlyFee['ProgMonthlyFee']['Amount']; ?>&nbsp;</td>
		<td><?php echo $progMonthlyFee['ProgMonthlyFee']['Active']; ?>&nbsp;</td>
		<td><?php echo $progMonthlyFee['ProgMonthlyFee']['Created']; ?>&nbsp;</td>
		<td><?php echo $progMonthlyFee['ProgMonthlyFee']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $progMonthlyFee['ProgMonthlyFee']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $progMonthlyFee['ProgMonthlyFee']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $progMonthlyFee['ProgMonthlyFee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progMonthlyFee['ProgMonthlyFee']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Prog Monthly Fee', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
	</ul>
</div>