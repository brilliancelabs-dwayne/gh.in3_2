<div class="progMonthlyFees view">
<h2><?php  __('Prog Monthly Fee');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progMonthlyFee['ProgMonthlyFee']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Prog'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($progMonthlyFee['Prog']['id'], array('controller' => 'progs', 'action' => 'view', $progMonthlyFee['Prog']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progMonthlyFee['ProgMonthlyFee']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progMonthlyFee['ProgMonthlyFee']['Amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progMonthlyFee['ProgMonthlyFee']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progMonthlyFee['ProgMonthlyFee']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progMonthlyFee['ProgMonthlyFee']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Prog Monthly Fee', true), array('action' => 'edit', $progMonthlyFee['ProgMonthlyFee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Prog Monthly Fee', true), array('action' => 'delete', $progMonthlyFee['ProgMonthlyFee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progMonthlyFee['ProgMonthlyFee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog Monthly Fees', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Monthly Fee', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
	</ul>
</div>
