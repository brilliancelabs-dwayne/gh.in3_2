<div class="progPeople form">
<?php echo $this->Form->create('ProgPerson');?>
	<fieldset>
		<legend><?php __('Add Prog Person'); ?></legend>
	<?php
		echo $this->Form->input('prog_id');
		echo $this->Form->input('person_id');
		echo $this->Form->input('Note');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Prog People', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List People', true), array('controller' => 'people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add')); ?> </li>
	</ul>
</div>