<div class="progPeople view">
<h2><?php  __('Prog Person');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progPerson['ProgPerson']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Prog'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($progPerson['Prog']['id'], array('controller' => 'progs', 'action' => 'view', $progPerson['Prog']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Person'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($progPerson['Person']['id'], array('controller' => 'people', 'action' => 'view', $progPerson['Person']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Note'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $progPerson['ProgPerson']['Note']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Prog Person', true), array('action' => 'edit', $progPerson['ProgPerson']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Prog Person', true), array('action' => 'delete', $progPerson['ProgPerson']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progPerson['ProgPerson']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Prog People', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog Person', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List People', true), array('controller' => 'people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add')); ?> </li>
	</ul>
</div>
