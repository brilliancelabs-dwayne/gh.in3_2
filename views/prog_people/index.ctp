<div class="progPeople index">
	<h2><?php __('Prog People');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('prog_id');?></th>
			<th><?php echo $this->Paginator->sort('person_id');?></th>
			<th><?php echo $this->Paginator->sort('Note');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($progPeople as $progPerson):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $progPerson['ProgPerson']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($progPerson['Prog']['id'], array('controller' => 'progs', 'action' => 'view', $progPerson['Prog']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($progPerson['Person']['id'], array('controller' => 'people', 'action' => 'view', $progPerson['Person']['id'])); ?>
		</td>
		<td><?php echo $progPerson['ProgPerson']['Note']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $progPerson['ProgPerson']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $progPerson['ProgPerson']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $progPerson['ProgPerson']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $progPerson['ProgPerson']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Prog Person', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List People', true), array('controller' => 'people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add')); ?> </li>
	</ul>
</div>