<div class="currentbills form">
<?php echo $this->Form->create('Currentbill');?>
	<fieldset>
		<legend><?php __('Add Currentbill'); ?></legend>
	<?php
		echo $this->Form->input('Name');
		echo $this->Form->input('Amount');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Currentbills', true), array('action' => 'index'));?></li>
	</ul>
</div>