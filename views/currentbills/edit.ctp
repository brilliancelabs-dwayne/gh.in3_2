<div class="currentbills form">
<?php echo $this->Form->create('Currentbill');?>
	<fieldset>
		<legend><?php __('Edit Currentbill'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Amount');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Currentbill.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Currentbill.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Currentbills', true), array('action' => 'index'));?></li>
	</ul>
</div>