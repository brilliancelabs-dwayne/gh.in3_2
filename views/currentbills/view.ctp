<div class="currentbills view">
<h2><?php  __('Currentbill');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $currentbill['Currentbill']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $currentbill['Currentbill']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $currentbill['Currentbill']['Amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $currentbill['Currentbill']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $currentbill['Currentbill']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $currentbill['Currentbill']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Currentbill', true), array('action' => 'edit', $currentbill['Currentbill']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Currentbill', true), array('action' => 'delete', $currentbill['Currentbill']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $currentbill['Currentbill']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Currentbills', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Currentbill', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
