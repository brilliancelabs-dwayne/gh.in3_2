<div class="merchants view">
<h2><?php  __('Merchant');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $merchant['Merchant']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $merchant['Merchant']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $merchant['Merchant']['Description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $merchant['Merchant']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $merchant['Merchant']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $merchant['Merchant']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Merchant', true), array('action' => 'edit', $merchant['Merchant']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Merchant', true), array('action' => 'delete', $merchant['Merchant']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $merchant['Merchant']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Merchants', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Merchant', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bill Payments', true), array('controller' => 'bill_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bill Payment', true), array('controller' => 'bill_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Bill Payments');?></h3>
	<?php if (!empty($merchant['BillPayment'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Bill Id'); ?></th>
		<th><?php __('Merchant Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('TransactionId'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($merchant['BillPayment'] as $billPayment):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $billPayment['id'];?></td>
			<td><?php echo $billPayment['bill_id'];?></td>
			<td><?php echo $billPayment['merchant_id'];?></td>
			<td><?php echo $billPayment['Name'];?></td>
			<td><?php echo $billPayment['Amount'];?></td>
			<td><?php echo $billPayment['Active'];?></td>
			<td><?php echo $billPayment['TransactionId'];?></td>
			<td><?php echo $billPayment['Created'];?></td>
			<td><?php echo $billPayment['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bill_payments', 'action' => 'view', $billPayment['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bill_payments', 'action' => 'edit', $billPayment['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bill_payments', 'action' => 'delete', $billPayment['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $billPayment['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bill Payment', true), array('controller' => 'bill_payments', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
