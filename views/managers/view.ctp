<div class="managers view">
<h2><?php  __('Manager');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $manager['Manager']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Person'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($manager['Person']['id'], array('controller' => 'people', 'action' => 'view', $manager['Person']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Note'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $manager['Manager']['Note']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Manager', true), array('action' => 'edit', $manager['Manager']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Manager', true), array('action' => 'delete', $manager['Manager']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $manager['Manager']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Managers', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Manager', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List People', true), array('controller' => 'people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Progs', true), array('controller' => 'progs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Progs');?></h3>
	<?php if (!empty($manager['Prog'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Description'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($manager['Prog'] as $prog):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $prog['id'];?></td>
			<td><?php echo $prog['Name'];?></td>
			<td><?php echo $prog['Description'];?></td>
			<td><?php echo $prog['Active'];?></td>
			<td><?php echo $prog['Created'];?></td>
			<td><?php echo $prog['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'progs', 'action' => 'view', $prog['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'progs', 'action' => 'edit', $prog['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'progs', 'action' => 'delete', $prog['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $prog['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Prog', true), array('controller' => 'progs', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
