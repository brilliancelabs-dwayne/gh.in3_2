<?php
/* Prog Test cases generated on: 2013-02-15 20:58:58 : 1360961938*/
App::import('Model', 'Prog');

class ProgTestCase extends CakeTestCase {
	var $fixtures = array('app.prog', 'app.prog_annual_fee', 'app.prog_manager', 'app.manager', 'app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.prog_person_monthly_fee', 'app.user', 'app.prog_person', 'app.prog_monthly_fee', 'app.prog_user_monthly_fee');

	function startTest() {
		$this->Prog =& ClassRegistry::init('Prog');
	}

	function endTest() {
		unset($this->Prog);
		ClassRegistry::flush();
	}

}
