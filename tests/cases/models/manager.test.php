<?php
/* Manager Test cases generated on: 2013-02-15 20:58:54 : 1360961934*/
App::import('Model', 'Manager');

class ManagerTestCase extends CakeTestCase {
	var $fixtures = array('app.manager', 'app.person', 'app.prog', 'app.prog_manager');

	function startTest() {
		$this->Manager =& ClassRegistry::init('Manager');
	}

	function endTest() {
		unset($this->Manager);
		ClassRegistry::flush();
	}

}
