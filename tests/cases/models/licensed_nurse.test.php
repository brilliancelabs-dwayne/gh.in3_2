<?php
/* LicensedNurse Test cases generated on: 2013-02-15 20:58:54 : 1360961934*/
App::import('Model', 'LicensedNurse');

class LicensedNurseTestCase extends CakeTestCase {
	var $fixtures = array('app.licensed_nurse', 'app.employee', 'app.person', 'app.aide', 'app.officeemployee', 'app.registered_nurse');

	function startTest() {
		$this->LicensedNurse =& ClassRegistry::init('LicensedNurse');
	}

	function endTest() {
		unset($this->LicensedNurse);
		ClassRegistry::flush();
	}

}
