<?php
/* User Test cases generated on: 2013-02-15 20:58:59 : 1360961939*/
App::import('Model', 'User');

class UserTestCase extends CakeTestCase {
	var $fixtures = array('app.user', 'app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.manager', 'app.prog', 'app.prog_annual_fee', 'app.prog_manager', 'app.prog_monthly_fee', 'app.prog_person', 'app.prog_person_monthly_fee', 'app.prog_user_monthly_fee');

	function startTest() {
		$this->User =& ClassRegistry::init('User');
	}

	function endTest() {
		unset($this->User);
		ClassRegistry::flush();
	}

}
