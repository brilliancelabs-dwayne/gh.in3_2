<?php
/* Balance Test cases generated on: 2013-02-15 20:58:52 : 1360961932*/
App::import('Model', 'Balance');

class BalanceTestCase extends CakeTestCase {
	var $fixtures = array('app.balance', 'app.bill', 'app.bill_payment');

	function startTest() {
		$this->Balance =& ClassRegistry::init('Balance');
	}

	function endTest() {
		unset($this->Balance);
		ClassRegistry::flush();
	}

}
