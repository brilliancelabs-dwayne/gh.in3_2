<?php
/* ProgAnnualFee Test cases generated on: 2013-02-15 20:58:56 : 1360961936*/
App::import('Model', 'ProgAnnualFee');

class ProgAnnualFeeTestCase extends CakeTestCase {
	var $fixtures = array('app.prog_annual_fee', 'app.prog');

	function startTest() {
		$this->ProgAnnualFee =& ClassRegistry::init('ProgAnnualFee');
	}

	function endTest() {
		unset($this->ProgAnnualFee);
		ClassRegistry::flush();
	}

}
