<?php
/* Merchant Test cases generated on: 2013-02-15 20:58:54 : 1360961934*/
App::import('Model', 'Merchant');

class MerchantTestCase extends CakeTestCase {
	var $fixtures = array('app.merchant', 'app.bill_payment', 'app.bill', 'app.balance');

	function startTest() {
		$this->Merchant =& ClassRegistry::init('Merchant');
	}

	function endTest() {
		unset($this->Merchant);
		ClassRegistry::flush();
	}

}
