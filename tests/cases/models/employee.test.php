<?php
/* Employee Test cases generated on: 2013-02-15 20:58:53 : 1360961933*/
App::import('Model', 'Employee');

class EmployeeTestCase extends CakeTestCase {
	var $fixtures = array('app.employee', 'app.person', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse');

	function startTest() {
		$this->Employee =& ClassRegistry::init('Employee');
	}

	function endTest() {
		unset($this->Employee);
		ClassRegistry::flush();
	}

}
