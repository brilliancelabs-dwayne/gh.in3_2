<?php
/* ProgPersonMonthlyFee Test cases generated on: 2013-02-15 20:58:57 : 1360961937*/
App::import('Model', 'ProgPersonMonthlyFee');

class ProgPersonMonthlyFeeTestCase extends CakeTestCase {
	var $fixtures = array('app.prog_person_monthly_fee', 'app.prog', 'app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.manager', 'app.prog_manager', 'app.user', 'app.prog_person');

	function startTest() {
		$this->ProgPersonMonthlyFee =& ClassRegistry::init('ProgPersonMonthlyFee');
	}

	function endTest() {
		unset($this->ProgPersonMonthlyFee);
		ClassRegistry::flush();
	}

}
