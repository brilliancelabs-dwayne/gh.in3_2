<?php
/* Person Test cases generated on: 2013-02-15 20:58:55 : 1360961935*/
App::import('Model', 'Person');

class PersonTestCase extends CakeTestCase {
	var $fixtures = array('app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.manager', 'app.prog', 'app.prog_manager', 'app.prog_person_monthly_fee', 'app.user', 'app.prog_person');

	function startTest() {
		$this->Person =& ClassRegistry::init('Person');
	}

	function endTest() {
		unset($this->Person);
		ClassRegistry::flush();
	}

}
