<?php
/* ProgManager Test cases generated on: 2013-02-15 20:58:56 : 1360961936*/
App::import('Model', 'ProgManager');

class ProgManagerTestCase extends CakeTestCase {
	var $fixtures = array('app.prog_manager', 'app.prog', 'app.manager', 'app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.prog_person_monthly_fee', 'app.user', 'app.prog_person');

	function startTest() {
		$this->ProgManager =& ClassRegistry::init('ProgManager');
	}

	function endTest() {
		unset($this->ProgManager);
		ClassRegistry::flush();
	}

}
