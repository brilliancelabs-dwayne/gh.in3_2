<?php
/* Aide Test cases generated on: 2013-02-15 20:58:51 : 1360961931*/
App::import('Model', 'Aide');

class AideTestCase extends CakeTestCase {
	var $fixtures = array('app.aide', 'app.employee');

	function startTest() {
		$this->Aide =& ClassRegistry::init('Aide');
	}

	function endTest() {
		unset($this->Aide);
		ClassRegistry::flush();
	}

}
