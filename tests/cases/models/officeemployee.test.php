<?php
/* Officeemployee Test cases generated on: 2013-02-15 20:58:55 : 1360961935*/
App::import('Model', 'Officeemployee');

class OfficeemployeeTestCase extends CakeTestCase {
	var $fixtures = array('app.officeemployee', 'app.employee', 'app.person', 'app.aide', 'app.licensed_nurse', 'app.registered_nurse');

	function startTest() {
		$this->Officeemployee =& ClassRegistry::init('Officeemployee');
	}

	function endTest() {
		unset($this->Officeemployee);
		ClassRegistry::flush();
	}

}
