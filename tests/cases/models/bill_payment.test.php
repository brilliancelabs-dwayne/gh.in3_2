<?php
/* BillPayment Test cases generated on: 2013-02-15 20:58:52 : 1360961932*/
App::import('Model', 'BillPayment');

class BillPaymentTestCase extends CakeTestCase {
	var $fixtures = array('app.bill_payment', 'app.bill', 'app.merchant', 'app.balance');

	function startTest() {
		$this->BillPayment =& ClassRegistry::init('BillPayment');
	}

	function endTest() {
		unset($this->BillPayment);
		ClassRegistry::flush();
	}

}
