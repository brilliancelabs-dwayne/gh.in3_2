<?php
/* ProgUserMonthlyFee Test cases generated on: 2013-02-15 20:58:58 : 1360961938*/
App::import('Model', 'ProgUserMonthlyFee');

class ProgUserMonthlyFeeTestCase extends CakeTestCase {
	var $fixtures = array('app.prog_user_monthly_fee', 'app.prog', 'app.user');

	function startTest() {
		$this->ProgUserMonthlyFee =& ClassRegistry::init('ProgUserMonthlyFee');
	}

	function endTest() {
		unset($this->ProgUserMonthlyFee);
		ClassRegistry::flush();
	}

}
