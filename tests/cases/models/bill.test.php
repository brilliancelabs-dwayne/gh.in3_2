<?php
/* Bill Test cases generated on: 2013-02-15 20:58:52 : 1360961932*/
App::import('Model', 'Bill');

class BillTestCase extends CakeTestCase {
	var $fixtures = array('app.bill', 'app.balance', 'app.bill_payment', 'app.merchant');

	function startTest() {
		$this->Bill =& ClassRegistry::init('Bill');
	}

	function endTest() {
		unset($this->Bill);
		ClassRegistry::flush();
	}

}
