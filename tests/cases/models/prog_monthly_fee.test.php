<?php
/* ProgMonthlyFee Test cases generated on: 2013-02-15 20:58:57 : 1360961937*/
App::import('Model', 'ProgMonthlyFee');

class ProgMonthlyFeeTestCase extends CakeTestCase {
	var $fixtures = array('app.prog_monthly_fee', 'app.prog');

	function startTest() {
		$this->ProgMonthlyFee =& ClassRegistry::init('ProgMonthlyFee');
	}

	function endTest() {
		unset($this->ProgMonthlyFee);
		ClassRegistry::flush();
	}

}
