<?php
/* RegisteredNurse Test cases generated on: 2013-02-15 20:58:59 : 1360961939*/
App::import('Model', 'RegisteredNurse');

class RegisteredNurseTestCase extends CakeTestCase {
	var $fixtures = array('app.registered_nurse', 'app.employee', 'app.person', 'app.manager', 'app.prog', 'app.prog_annual_fee', 'app.prog_manager', 'app.prog_monthly_fee', 'app.prog_person', 'app.prog_person_monthly_fee', 'app.prog_user_monthly_fee', 'app.user', 'app.aide', 'app.licensed_nurse', 'app.officeemployee');

	function startTest() {
		$this->RegisteredNurse =& ClassRegistry::init('RegisteredNurse');
	}

	function endTest() {
		unset($this->RegisteredNurse);
		ClassRegistry::flush();
	}

}
