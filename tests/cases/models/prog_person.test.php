<?php
/* ProgPerson Test cases generated on: 2013-02-15 20:58:57 : 1360961937*/
App::import('Model', 'ProgPerson');

class ProgPersonTestCase extends CakeTestCase {
	var $fixtures = array('app.prog_person', 'app.prog', 'app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.manager', 'app.prog_manager', 'app.prog_person_monthly_fee', 'app.user');

	function startTest() {
		$this->ProgPerson =& ClassRegistry::init('ProgPerson');
	}

	function endTest() {
		unset($this->ProgPerson);
		ClassRegistry::flush();
	}

}
