<?php
/* People Test cases generated on: 2013-02-15 20:59:00 : 1360961940*/
App::import('Controller', 'People');

class TestPeopleController extends PeopleController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PeopleControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.manager', 'app.prog', 'app.prog_annual_fee', 'app.prog_manager', 'app.prog_monthly_fee', 'app.prog_person', 'app.prog_person_monthly_fee', 'app.prog_user_monthly_fee', 'app.user');

	function startTest() {
		$this->People =& new TestPeopleController();
		$this->People->constructClasses();
	}

	function endTest() {
		unset($this->People);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
