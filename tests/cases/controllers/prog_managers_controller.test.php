<?php
/* ProgManagers Test cases generated on: 2013-02-15 20:59:00 : 1360961940*/
App::import('Controller', 'ProgManagers');

class TestProgManagersController extends ProgManagersController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ProgManagersControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.prog_manager', 'app.prog', 'app.prog_annual_fee', 'app.prog_monthly_fee', 'app.prog_person', 'app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.manager', 'app.prog_person_monthly_fee', 'app.user', 'app.prog_user_monthly_fee');

	function startTest() {
		$this->ProgManagers =& new TestProgManagersController();
		$this->ProgManagers->constructClasses();
	}

	function endTest() {
		unset($this->ProgManagers);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
