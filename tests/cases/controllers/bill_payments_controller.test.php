<?php
/* BillPayments Test cases generated on: 2013-02-15 20:58:59 : 1360961939*/
App::import('Controller', 'BillPayments');

class TestBillPaymentsController extends BillPaymentsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BillPaymentsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.bill_payment', 'app.bill', 'app.balance', 'app.merchant');

	function startTest() {
		$this->BillPayments =& new TestBillPaymentsController();
		$this->BillPayments->constructClasses();
	}

	function endTest() {
		unset($this->BillPayments);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
