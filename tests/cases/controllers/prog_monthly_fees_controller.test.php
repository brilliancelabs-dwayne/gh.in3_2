<?php
/* ProgMonthlyFees Test cases generated on: 2013-02-15 20:59:00 : 1360961940*/
App::import('Controller', 'ProgMonthlyFees');

class TestProgMonthlyFeesController extends ProgMonthlyFeesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ProgMonthlyFeesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.prog_monthly_fee', 'app.prog', 'app.prog_annual_fee', 'app.prog_manager', 'app.manager', 'app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.prog_person_monthly_fee', 'app.user', 'app.prog_user_monthly_fee', 'app.prog_person');

	function startTest() {
		$this->ProgMonthlyFees =& new TestProgMonthlyFeesController();
		$this->ProgMonthlyFees->constructClasses();
	}

	function endTest() {
		unset($this->ProgMonthlyFees);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
