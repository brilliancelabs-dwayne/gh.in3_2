<?php
/* Managers Test cases generated on: 2013-02-15 20:59:00 : 1360961940*/
App::import('Controller', 'Managers');

class TestManagersController extends ManagersController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ManagersControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.manager', 'app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.prog_person_monthly_fee', 'app.prog', 'app.prog_annual_fee', 'app.prog_manager', 'app.prog_monthly_fee', 'app.prog_person', 'app.prog_user_monthly_fee', 'app.user');

	function startTest() {
		$this->Managers =& new TestManagersController();
		$this->Managers->constructClasses();
	}

	function endTest() {
		unset($this->Managers);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
