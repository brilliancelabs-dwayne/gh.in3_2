<?php
/* Merchants Test cases generated on: 2013-02-15 20:59:00 : 1360961940*/
App::import('Controller', 'Merchants');

class TestMerchantsController extends MerchantsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MerchantsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.merchant', 'app.bill_payment', 'app.bill', 'app.balance');

	function startTest() {
		$this->Merchants =& new TestMerchantsController();
		$this->Merchants->constructClasses();
	}

	function endTest() {
		unset($this->Merchants);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
