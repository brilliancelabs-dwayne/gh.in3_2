<?php
/* Bills Test cases generated on: 2013-02-15 20:58:59 : 1360961939*/
App::import('Controller', 'Bills');

class TestBillsController extends BillsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BillsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.bill', 'app.balance', 'app.bill_payment', 'app.merchant');

	function startTest() {
		$this->Bills =& new TestBillsController();
		$this->Bills->constructClasses();
	}

	function endTest() {
		unset($this->Bills);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
