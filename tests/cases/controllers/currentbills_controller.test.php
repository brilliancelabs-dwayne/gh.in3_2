<?php
/* Currentbills Test cases generated on: 2013-02-15 20:58:59 : 1360961939*/
App::import('Controller', 'Currentbills');

class TestCurrentbillsController extends CurrentbillsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class CurrentbillsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.currentbill');

	function startTest() {
		$this->Currentbills =& new TestCurrentbillsController();
		$this->Currentbills->constructClasses();
	}

	function endTest() {
		unset($this->Currentbills);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
