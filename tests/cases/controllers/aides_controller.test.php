<?php
/* Aides Test cases generated on: 2013-02-15 20:58:59 : 1360961939*/
App::import('Controller', 'Aides');

class TestAidesController extends AidesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class AidesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.aide', 'app.employee', 'app.person', 'app.manager', 'app.prog', 'app.prog_annual_fee', 'app.prog_manager', 'app.prog_monthly_fee', 'app.prog_person', 'app.prog_person_monthly_fee', 'app.prog_user_monthly_fee', 'app.user', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse');

	function startTest() {
		$this->Aides =& new TestAidesController();
		$this->Aides->constructClasses();
	}

	function endTest() {
		unset($this->Aides);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
