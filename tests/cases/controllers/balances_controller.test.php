<?php
/* Balances Test cases generated on: 2013-02-15 20:58:59 : 1360961939*/
App::import('Controller', 'Balances');

class TestBalancesController extends BalancesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BalancesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.balance', 'app.bill', 'app.bill_payment', 'app.merchant');

	function startTest() {
		$this->Balances =& new TestBalancesController();
		$this->Balances->constructClasses();
	}

	function endTest() {
		unset($this->Balances);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
