<?php
/* Officeemployees Test cases generated on: 2013-02-15 20:59:00 : 1360961940*/
App::import('Controller', 'Officeemployees');

class TestOfficeemployeesController extends OfficeemployeesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class OfficeemployeesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.officeemployee', 'app.employee', 'app.person', 'app.manager', 'app.prog', 'app.prog_annual_fee', 'app.prog_manager', 'app.prog_monthly_fee', 'app.prog_person', 'app.prog_person_monthly_fee', 'app.prog_user_monthly_fee', 'app.user', 'app.aide', 'app.licensed_nurse', 'app.registered_nurse');

	function startTest() {
		$this->Officeemployees =& new TestOfficeemployeesController();
		$this->Officeemployees->constructClasses();
	}

	function endTest() {
		unset($this->Officeemployees);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
