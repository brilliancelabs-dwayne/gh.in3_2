<?php
/* ProgPeople Test cases generated on: 2013-02-15 20:59:00 : 1360961940*/
App::import('Controller', 'ProgPeople');

class TestProgPeopleController extends ProgPeopleController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ProgPeopleControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.prog_person', 'app.prog', 'app.prog_annual_fee', 'app.prog_manager', 'app.manager', 'app.person', 'app.employee', 'app.aide', 'app.licensed_nurse', 'app.officeemployee', 'app.registered_nurse', 'app.prog_person_monthly_fee', 'app.user', 'app.prog_user_monthly_fee', 'app.prog_monthly_fee');

	function startTest() {
		$this->ProgPeople =& new TestProgPeopleController();
		$this->ProgPeople->constructClasses();
	}

	function endTest() {
		unset($this->ProgPeople);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
