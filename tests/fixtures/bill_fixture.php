<?php
/* Bill Fixture generated on: 2013-02-15 20:58:52 : 1360961932 */
class BillFixture extends CakeTestFixture {
	var $name = 'Bill';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'Name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 75, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'EventDate' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'Description' => array('type' => 'text', 'null' => true, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Active' => array('type' => 'string', 'null' => true, 'default' => '1', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Amount' => array('type' => 'float', 'null' => false, 'default' => '0.00', 'length' => '10,2'),
		'Created' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'Updated' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'Name' => 'Lorem ipsum dolor sit amet',
			'EventDate' => '2013-02-15',
			'Description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'Active' => 'Lorem ipsum dolor sit ame',
			'Amount' => 1,
			'Created' => '2013-02-15',
			'Updated' => 1360961932
		),
	);
}
