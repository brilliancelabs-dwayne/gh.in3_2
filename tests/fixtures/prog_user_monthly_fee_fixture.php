<?php
/* ProgUserMonthlyFee Fixture generated on: 2013-02-15 20:58:58 : 1360961938 */
class ProgUserMonthlyFeeFixture extends CakeTestFixture {
	var $name = 'ProgUserMonthlyFee';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'prog_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'Name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 75, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Amount' => array('type' => 'float', 'null' => false, 'default' => NULL, 'length' => '10,2'),
		'Active' => array('type' => 'string', 'null' => true, 'default' => '1', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Created' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'Updated' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'DynKey' => array('column' => array('prog_id', 'user_id'), 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'prog_id' => 1,
			'user_id' => 1,
			'Name' => 'Lorem ipsum dolor sit amet',
			'Amount' => 1,
			'Active' => 'Lorem ipsum dolor sit ame',
			'Created' => '2013-02-15',
			'Updated' => 1360961938
		),
	);
}
