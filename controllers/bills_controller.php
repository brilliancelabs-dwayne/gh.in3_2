<?php
class BillsController extends AppController {

	var $name = 'Bills';

	function index() {
		$this->Bill->recursive = 0;
		$this->set('bills', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid bill', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('bill', $this->Bill->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Bill->create();
			if ($this->Bill->save($this->data)) {
				$this->Session->setFlash(__('The bill has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bill could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid bill', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Bill->save($this->data)) {
				$this->Session->setFlash(__('The bill has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bill could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Bill->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for bill', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Bill->delete($id)) {
			$this->Session->setFlash(__('Bill deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Bill was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
