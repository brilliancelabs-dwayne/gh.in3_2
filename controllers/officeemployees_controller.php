<?php
class OfficeemployeesController extends AppController {

	var $name = 'Officeemployees';

	function index() {
		$this->Officeemployee->recursive = 0;
		$this->set('officeemployees', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid officeemployee', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('officeemployee', $this->Officeemployee->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Officeemployee->create();
			if ($this->Officeemployee->save($this->data)) {
				$this->Session->setFlash(__('The officeemployee has been saved', true));
				$this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('The officeemployee could not be saved. Please, try again.', true));
			}
		}
		$employees = $this->Officeemployee->Employee->find('list');
		$this->set(compact('employees'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid officeemployee', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Officeemployee->save($this->data)) {
				$this->Session->setFlash(__('The officeemployee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The officeemployee could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Officeemployee->read(null, $id);
		}
		$employees = $this->Officeemployee->Employee->find('list');
		$this->set(compact('employees'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for officeemployee', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Officeemployee->delete($id)) {
			$this->Session->setFlash(__('Officeemployee deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Officeemployee was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
