<?php
class ProgPersonMonthlyFeesController extends AppController {

	var $name = 'ProgPersonMonthlyFees';

	function index() {
		$this->ProgPersonMonthlyFee->recursive = 0;
		$this->set('progPersonMonthlyFees', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid prog person monthly fee', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('progPersonMonthlyFee', $this->ProgPersonMonthlyFee->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProgPersonMonthlyFee->create();
			if ($this->ProgPersonMonthlyFee->save($this->data)) {
				$this->Session->setFlash(__('The prog person monthly fee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog person monthly fee could not be saved. Please, try again.', true));
			}
		}
		$progs = $this->ProgPersonMonthlyFee->Prog->find('list');
		$people = $this->ProgPersonMonthlyFee->Person->find('list');
		$this->set(compact('progs', 'people'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid prog person monthly fee', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProgPersonMonthlyFee->save($this->data)) {
				$this->Session->setFlash(__('The prog person monthly fee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog person monthly fee could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProgPersonMonthlyFee->read(null, $id);
		}
		$progs = $this->ProgPersonMonthlyFee->Prog->find('list');
		$people = $this->ProgPersonMonthlyFee->Person->find('list');
		$this->set(compact('progs', 'people'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for prog person monthly fee', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProgPersonMonthlyFee->delete($id)) {
			$this->Session->setFlash(__('Prog person monthly fee deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Prog person monthly fee was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
