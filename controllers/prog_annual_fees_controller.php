<?php
class ProgAnnualFeesController extends AppController {

	var $name = 'ProgAnnualFees';

	function index() {
		$this->ProgAnnualFee->recursive = 0;
		$this->set('progAnnualFees', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid prog annual fee', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('progAnnualFee', $this->ProgAnnualFee->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProgAnnualFee->create();
			if ($this->ProgAnnualFee->save($this->data)) {
				$this->Session->setFlash(__('The prog annual fee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog annual fee could not be saved. Please, try again.', true));
			}
		}
		$progs = $this->ProgAnnualFee->Prog->find('list');
		$this->set(compact('progs'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid prog annual fee', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProgAnnualFee->save($this->data)) {
				$this->Session->setFlash(__('The prog annual fee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog annual fee could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProgAnnualFee->read(null, $id);
		}
		$progs = $this->ProgAnnualFee->Prog->find('list');
		$this->set(compact('progs'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for prog annual fee', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProgAnnualFee->delete($id)) {
			$this->Session->setFlash(__('Prog annual fee deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Prog annual fee was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
