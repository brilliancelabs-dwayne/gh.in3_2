<?php
class ProgMonthlyFeesController extends AppController {

	var $name = 'ProgMonthlyFees';

	function index() {
		$this->ProgMonthlyFee->recursive = 0;
		$this->set('progMonthlyFees', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid prog monthly fee', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('progMonthlyFee', $this->ProgMonthlyFee->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProgMonthlyFee->create();
			if ($this->ProgMonthlyFee->save($this->data)) {
				$this->Session->setFlash(__('The prog monthly fee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog monthly fee could not be saved. Please, try again.', true));
			}
		}
		$progs = $this->ProgMonthlyFee->Prog->find('list');
		$this->set(compact('progs'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid prog monthly fee', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProgMonthlyFee->save($this->data)) {
				$this->Session->setFlash(__('The prog monthly fee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog monthly fee could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProgMonthlyFee->read(null, $id);
		}
		$progs = $this->ProgMonthlyFee->Prog->find('list');
		$this->set(compact('progs'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for prog monthly fee', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProgMonthlyFee->delete($id)) {
			$this->Session->setFlash(__('Prog monthly fee deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Prog monthly fee was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
