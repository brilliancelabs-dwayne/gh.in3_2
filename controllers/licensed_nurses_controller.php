<?php
class LicensedNursesController extends AppController {

	var $name = 'LicensedNurses';

	function index() {
		$this->LicensedNurse->recursive = 0;
		$this->set('licensedNurses', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid licensed nurse', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('licensedNurse', $this->LicensedNurse->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->LicensedNurse->create();
			if ($this->LicensedNurse->save($this->data)) {
				$this->Session->setFlash(__('The licensed nurse has been saved', true));
				$this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('The licensed nurse could not be saved. Please, try again.', true));
			}
		}
		$employees = $this->LicensedNurse->Employee->find('list');
		$this->set(compact('employees'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid licensed nurse', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->LicensedNurse->save($this->data)) {
				$this->Session->setFlash(__('The licensed nurse has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The licensed nurse could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->LicensedNurse->read(null, $id);
		}
		$employees = $this->LicensedNurse->Employee->find('list');
		$this->set(compact('employees'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for licensed nurse', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->LicensedNurse->delete($id)) {
			$this->Session->setFlash(__('Licensed nurse deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Licensed nurse was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
