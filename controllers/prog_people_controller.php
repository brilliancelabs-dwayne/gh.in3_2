<?php
class ProgPeopleController extends AppController {

	var $name = 'ProgPeople';

	function index() {
		$this->ProgPerson->recursive = 0;
		$this->set('progPeople', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid prog person', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('progPerson', $this->ProgPerson->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProgPerson->create();
			if ($this->ProgPerson->save($this->data)) {
				$this->Session->setFlash(__('The prog person has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog person could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid prog person', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProgPerson->save($this->data)) {
				$this->Session->setFlash(__('The prog person has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog person could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProgPerson->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for prog person', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProgPerson->delete($id)) {
			$this->Session->setFlash(__('Prog person deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Prog person was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
