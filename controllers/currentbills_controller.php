<?php
class CurrentbillsController extends AppController {

	var $name = 'Currentbills';

	function index() {
		$this->Currentbill->recursive = 0;
		$this->set('currentbills', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid currentbill', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('currentbill', $this->Currentbill->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Currentbill->create();
			if ($this->Currentbill->save($this->data)) {
				$this->Session->setFlash(__('The currentbill has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The currentbill could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid currentbill', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Currentbill->save($this->data)) {
				$this->Session->setFlash(__('The currentbill has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The currentbill could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Currentbill->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for currentbill', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Currentbill->delete($id)) {
			$this->Session->setFlash(__('Currentbill deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Currentbill was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
