<?php
class ManagersController extends AppController {

	var $name = 'Managers';

	function index() {
		$this->Manager->recursive = 0;
		$this->set('managers', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manager', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manager', $this->Manager->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Manager->create();
			if ($this->Manager->save($this->data)) {
				$this->Session->setFlash(__('The manager has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manager could not be saved. Please, try again.', true));
			}
		}
		$people = $this->Manager->Person->find('list');
		$progs = $this->Manager->Prog->find('list');
		$this->set(compact('people', 'progs'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manager', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Manager->save($this->data)) {
				$this->Session->setFlash(__('The manager has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manager could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Manager->read(null, $id);
		}
		$people = $this->Manager->Person->find('list');
		$progs = $this->Manager->Prog->find('list');
		$this->set(compact('people', 'progs'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manager', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Manager->delete($id)) {
			$this->Session->setFlash(__('Manager deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manager was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
