<?php
class AidesController extends AppController {

	var $name = 'Aides';

	function index() {
		$this->Aide->recursive = 0;
		$this->set('aides', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid aide', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('aide', $this->Aide->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Aide->create();
			if ($this->Aide->save($this->data)) {
				$this->Session->setFlash(__('The aide has been saved', true));
				$this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('The aide could not be saved. Please, try again.', true));
			}
		}
		$employees = $this->Aide->Employee->find('list');
		$this->set(compact('employees'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid aide', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Aide->save($this->data)) {
				$this->Session->setFlash(__('The aide has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The aide could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Aide->read(null, $id);
		}
		$employees = $this->Aide->Employee->find('list');
		$this->set(compact('employees'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for aide', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Aide->delete($id)) {
			$this->Session->setFlash(__('Aide deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Aide was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
