<?php
class ProgUserMonthlyFeesController extends AppController {

	var $name = 'ProgUserMonthlyFees';

	function index() {
		$this->ProgUserMonthlyFee->recursive = 0;
		$this->set('progUserMonthlyFees', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid prog user monthly fee', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('progUserMonthlyFee', $this->ProgUserMonthlyFee->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProgUserMonthlyFee->create();
			if ($this->ProgUserMonthlyFee->save($this->data)) {
				$this->Session->setFlash(__('The prog user monthly fee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog user monthly fee could not be saved. Please, try again.', true));
			}
		}
		$progs = $this->ProgUserMonthlyFee->Prog->find('list');
		$users = $this->ProgUserMonthlyFee->User->find('list');
		$this->set(compact('progs', 'users'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid prog user monthly fee', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProgUserMonthlyFee->save($this->data)) {
				$this->Session->setFlash(__('The prog user monthly fee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog user monthly fee could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProgUserMonthlyFee->read(null, $id);
		}
		$progs = $this->ProgUserMonthlyFee->Prog->find('list');
		$users = $this->ProgUserMonthlyFee->User->find('list');
		$this->set(compact('progs', 'users'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for prog user monthly fee', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProgUserMonthlyFee->delete($id)) {
			$this->Session->setFlash(__('Prog user monthly fee deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Prog user monthly fee was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
