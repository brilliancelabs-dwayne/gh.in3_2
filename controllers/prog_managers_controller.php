<?php
class ProgManagersController extends AppController {

	var $name = 'ProgManagers';

	function index() {
		$this->ProgManager->recursive = 0;
		$this->set('progManagers', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid prog manager', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('progManager', $this->ProgManager->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProgManager->create();
			if ($this->ProgManager->save($this->data)) {
				$this->Session->setFlash(__('The prog manager has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog manager could not be saved. Please, try again.', true));
			}
		}
		$progs = $this->ProgManager->Prog->find('list');
		$managers = $this->ProgManager->Manager->find('list');
		$this->set(compact('progs', 'managers'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid prog manager', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProgManager->save($this->data)) {
				$this->Session->setFlash(__('The prog manager has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog manager could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProgManager->read(null, $id);
		}
		$progs = $this->ProgManager->Prog->find('list');
		$managers = $this->ProgManager->Manager->find('list');
		$this->set(compact('progs', 'managers'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for prog manager', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProgManager->delete($id)) {
			$this->Session->setFlash(__('Prog manager deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Prog manager was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
