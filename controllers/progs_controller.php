<?php
class ProgsController extends AppController {

	var $name = 'Progs';

	function index() {
		$this->Prog->recursive = 0;
		$this->set('progs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid prog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('prog', $this->Prog->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Prog->create();
			if ($this->Prog->save($this->data)) {
				$this->Session->setFlash(__('The prog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid prog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Prog->save($this->data)) {
				$this->Session->setFlash(__('The prog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Prog->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for prog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Prog->delete($id)) {
			$this->Session->setFlash(__('Prog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Prog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
