<?php
class BillPaymentsController extends AppController {

	var $name = 'BillPayments';

	function index() {
		$this->BillPayment->recursive = 0;
		$this->set('billPayments', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid bill payment', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('billPayment', $this->BillPayment->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->BillPayment->create();
			if ($this->BillPayment->save($this->data)) {
				$this->Session->setFlash(__('The bill payment has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bill payment could not be saved. Please, try again.', true));
			}
		}
		$bills = $this->BillPayment->Bill->find('list');
		$merchants = $this->BillPayment->Merchant->find('list');
		$this->set(compact('bills', 'merchants'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid bill payment', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->BillPayment->save($this->data)) {
				$this->Session->setFlash(__('The bill payment has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bill payment could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->BillPayment->read(null, $id);
		}
		$bills = $this->BillPayment->Bill->find('list');
		$merchants = $this->BillPayment->Merchant->find('list');
		$this->set(compact('bills', 'merchants'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for bill payment', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->BillPayment->delete($id)) {
			$this->Session->setFlash(__('Bill payment deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Bill payment was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
