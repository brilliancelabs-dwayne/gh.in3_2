<?php
class RegisteredNursesController extends AppController {

	var $name = 'RegisteredNurses';

	function index() {
		$this->RegisteredNurse->recursive = 0;
		$this->set('registeredNurses', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid registered nurse', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('registeredNurse', $this->RegisteredNurse->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->RegisteredNurse->create();
			if ($this->RegisteredNurse->save($this->data)) {
				$this->Session->setFlash(__('The registered nurse has been saved', true));
				$this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('The registered nurse could not be saved. Please, try again.', true));
			}
		}
		$employees = $this->RegisteredNurse->Employee->find('list');
		$this->set(compact('employees'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid registered nurse', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->RegisteredNurse->save($this->data)) {
				$this->Session->setFlash(__('The registered nurse has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registered nurse could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->RegisteredNurse->read(null, $id);
		}
		$employees = $this->RegisteredNurse->Employee->find('list');
		$this->set(compact('employees'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for registered nurse', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->RegisteredNurse->delete($id)) {
			$this->Session->setFlash(__('Registered nurse deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Registered nurse was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
